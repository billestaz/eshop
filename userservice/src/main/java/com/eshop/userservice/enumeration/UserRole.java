package com.eshop.userservice.enumeration;

public enum UserRole {

    CUSTOMER("ROLE_CUSTOMER"),
    SHOP_ASSISTANT("ROLE_SHOP_ASSISTANT");

    private final String role;

    UserRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return role;
    }
}
