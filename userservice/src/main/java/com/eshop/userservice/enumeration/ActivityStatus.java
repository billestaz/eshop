package com.eshop.userservice.enumeration;

public enum ActivityStatus {

    ACTIVE("ACTIVE"),
    INACTIVE("INACTIVE");

    private final String status;

    ActivityStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return status;
    }
}
