package com.eshop.userservice.entity;

import com.eshop.userservice.enumeration.ActivityStatus;
import com.eshop.userservice.enumeration.UserRole;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@NoArgsConstructor
public class AppUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    @Column(unique = true)
    private String email;
    private String password;
    @Enumerated(EnumType.STRING)
    private ActivityStatus status;
    @Enumerated(EnumType.STRING)
    private UserRole role;
    private Long warehouseId;

    @OneToOne
    @JoinColumn(name = "address_id")
    private Address address;
}
