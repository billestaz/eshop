--liquibase formatted sql
--changeset bpajak:1

INSERT INTO address (city, house_number, street, zip_code) VALUES ('Lodz', '38/40', 'Karolewska', '90-561');
INSERT INTO address (city, house_number, street, zip_code) VALUES ('Białystok', '17', 'Szkolna', '15-640');