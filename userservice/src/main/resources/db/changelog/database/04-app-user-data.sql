--liquibase formatted sql
--changeset bpajak:1

INSERT INTO app_user (email, first_name, last_name, password, role, status, warehouse_id, address_id) VALUES ('email@email.com', 'Bartek', 'Pająk', 'password', 'ROLE_SHOP_ASSISTANT', 'ACTIVE', 1, 1);
INSERT INTO app_user (email, first_name, last_name, password, role, status, warehouse_id, address_id) VALUES ('email2@email.com', 'Wojtek', 'Suchodolski', 'password', 'ROLE_CUSTOMER', 'ACTIVE', 1, 2);