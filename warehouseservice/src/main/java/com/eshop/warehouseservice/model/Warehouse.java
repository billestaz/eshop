package com.eshop.warehouseservice.model;

import com.eshop.warehouseservice.model.enums.WarehouseType;
import lombok.*;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Warehouse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    private WarehouseType type;

    @OneToOne
    @JoinColumn(name = "location_id", unique = true)
    private Location location;
}
