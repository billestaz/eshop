package com.eshop.warehouseservice.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Embeddable
public class StockPrimaryKeyId implements Serializable {
    private Long warehouseId;

    private Long productId;
}