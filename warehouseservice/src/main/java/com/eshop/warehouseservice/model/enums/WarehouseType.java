package com.eshop.warehouseservice.model.enums;

public enum WarehouseType {
    WAREHOUSE("WAREHOUSE"),
    SHOP("SHOP");

    private final String type;

    WarehouseType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}