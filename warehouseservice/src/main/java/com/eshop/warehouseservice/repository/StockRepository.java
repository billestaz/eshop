package com.eshop.warehouseservice.repository;

import com.eshop.warehouseservice.model.Stock;
import com.eshop.warehouseservice.model.StockPrimaryKeyId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockRepository extends JpaRepository<Stock, StockPrimaryKeyId> {

}
