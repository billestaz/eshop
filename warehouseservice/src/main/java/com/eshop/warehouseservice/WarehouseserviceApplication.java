package com.eshop.warehouseservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class WarehouseserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarehouseserviceApplication.class, args);
	}

}

