CREATE TABLE stock (
    product_id BIGINT NOT NULL,
    warehouse_id BIGINT NOT NULL,
    quantity INTEGER,
    PRIMARY KEY (product_id, warehouse_id)
);

ALTER TABLE stock
    ADD CONSTRAINT FK_stock_warehouse
    FOREIGN KEY (warehouse_id)
    REFERENCES warehouse;