INSERT INTO location (city, street, house_number, zip_code) VALUES
    ('Lodz', 'Pomorska', '18/19', '90-432'),
    ('Warszawa', 'Wolska', '25', '00-234'),
    ('Lodz', 'Piotrkowska', '56', '90-079'),
    ('Warszawa', 'Nowy swiat', '23', '00-089'),
    ('Wroclaw', '3 maja', '89/90', '50-231');