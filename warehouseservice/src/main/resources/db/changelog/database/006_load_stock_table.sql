INSERT INTO stock (product_id, warehouse_id, quantity) VALUES
    (1, 1, 50),
    (2, 1, 100),
    (3, 1, 150),
    (1, 2, 45),
    (1, 5, 80),
    (5, 2, 20),
    (5, 5, 30),
    (4, 1, 15),
    (4, 3, 20);