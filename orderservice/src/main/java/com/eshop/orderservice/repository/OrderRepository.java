package com.eshop.orderservice.repository;

import com.eshop.orderservice.model.UserOrder;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<UserOrder,Long> {
}
