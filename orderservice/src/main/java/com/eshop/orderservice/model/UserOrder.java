package com.eshop.orderservice.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserOrder {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long id;
    private LocalDateTime date;
    private Long userId;
    @OneToMany
    private List<OrderItem> items;
    @Enumerated(EnumType.STRING)
    private OrderStatus orderStatus;
    private Long warehouseId;
    private String deliveryType;
    private String paymentType;
}
