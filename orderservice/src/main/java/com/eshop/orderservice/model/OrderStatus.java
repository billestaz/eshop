package com.eshop.orderservice.model;

public enum OrderStatus {
    CANCELLED, PAID, IN_PROGRESS, COMPLETED, SHIPPED
}
