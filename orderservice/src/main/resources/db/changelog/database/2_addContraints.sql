--liquibase formatted sql
--changeset pmarkowski:2
alter table user_order_item add constraint FK_USER_ORDER_ITEMS_fk_ITEMS_ID_ref_ORDER_ITEM foreign key (item_id) references order_item;
alter table user_order_item add constraint FK_USER_ORDER_ITEMS_fk_USER_ORDER_ID_ref_USER_ORDER foreign key (user_order_id) references user_order;