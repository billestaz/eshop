package com.eshop.productservice.repository;

import com.eshop.productservice.model.Price;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PriceRepository extends JpaRepository <Price, Long>{
}
