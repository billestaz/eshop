--liquibase formatted sql
--changeset msiadul:3

INSERT INTO product (name, description, parent_id)
VALUES ('Dress', 'a dress from the newest collection', null);

INSERT INTO product (name, description, parent_id)
VALUES ('Jacket', 'a jacket from the newest collection', null);

INSERT INTO product (name, description, parent_id)
VALUES ('Red Dress', 'a red variant of our most popular dress', 1);

INSERT INTO price (effective_date, price_value, tax_percentage, product_id)
VALUES ('2021-09-17', 20.00, 10, 1);

INSERT INTO price (effective_date, price_value, tax_percentage, product_id)
VALUES ('2021-09-16', 30.00, 10, 2);

INSERT INTO property (property, type, unit, property_value, product_id)
VALUES ('color', 'STRING', null, 'red', 3 );

